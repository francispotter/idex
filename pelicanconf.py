AUTHOR = 'Francis Potter'
SITENAME = 'Paul Strasburg and the Origins of IDEX'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Vancouver'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('History', '/'),
         ('Soundtrack', '/pages/soundtrack.html'),
         ('Photos', '/pages/photos.html'),
         ('Making', '/pages/making.html'),
         ('Thanks', '/pages/thanks.html')
         )

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "themes/pelican-bootstrap3"
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGINS = ['plugins.i18n_subsites']
# MENUITEMS = [("Documentation","/documentation")]
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
# CATEGORY_URL = "category/{slug}.html"
USE_FOLDER_AS_CATEGORY = True
STATIC_PATHS = ['media']