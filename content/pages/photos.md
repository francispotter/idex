Title: Photos

The photos below appeared in the slideshow during the IDEX 10th anniversary dinner. They came from a collection of slides that volunteers and early staff members donated. As with the soundtrack, we've lost some of the information about the images, including the names of the photographers.

1993 Ghana trip
----------

![Image](/media/someone-mutombo-paul-tim-ghana.png)

Issahaku Jessiwuni, Mutombo Mpanya, Paul, and Tim McMains in Tamale, Ghana. Mr. Jessiwuni ran an IDEX partner organization.

![Image](/media/Dancing%20outdoors.png)

Paul spending time with Mr. Jessiwuni and the local community

![Image](/media/Ammasachina.png)

Paul and others in front of the office of the Ammasachina Self-Help Association

![Image](/media/Planting.png)

Paul planting some demonstration crops


![Image](/media/Mutombo%20bottle.png)

Mutombo Mpanya with a bottle on the head (listen to the soundtrack for the full story)

![Image](/media/Bowl%20on%20head.png)

Paul with a pot on his head  (listen to the soundtrack for the full story)


IDEX office and volunteer meetings
------------

![Image](/media/Three%20in%20Office.png)

Staff in the IDEX office: Becky Buell, Beth Foster, and Susan Beaudry

![Image](/media/Meeting1.png)

Volunteer meeting

![Image](/media/Meeting2.png)

Volunteer meeting


Education program
--------------

IDEX staff and volunteers taught about international social justice at schools in the Bay Area.

![Image](/media/Kid%20with%20globe.png)

![Image](/media/Kids%20with%20countries.png)

![Image](/media/kids-us-classroom.png)


Volunteers in Asia
-----------------

The organization that sent volunteers to countries in Asia, part of the Consortium for Global Development with IDEX and Overseas Development Network

![Image](/media/dwight-clark.png)

VIA founder Dwight Clark

![Image](/media/VIA%20staff.png)

Paul with the VIA staff on the Stanford campus, where VIA's office was located

Paul
----

![Image](/media/yearbook.png)

Dwight and Paul in the Stanford yearbook

![Image](/media/Car.png)

Standing next to a car

![Image](/media/Birthday.png)

Celebrating a birthday

Scenery
-----

Indonesia? Thailand? Used as background scenery in the slideshow

![Image](/media/indonesia-or-thailand-2.png)

![Image](/media/indonesia-or-thailand.png)

<!--![Image](/media/some-kids-in-dirt.png)-->

**[Learn about the making of the slideshow](/pages/making.html)**