Title: Thanks

Countless people contributed to the original dinner presentation and this web site, including all the participants and photographers who provided content, as well as:

- Paula Morris, former Executive Director, who invited me to make the slideshow
- Martha Hannan, former Development Director, who organized the dinner event
- Rajiv Khanna, Cindy del Rosario-Tapan, and Jessie Spector from the Thousand Currents staff, who provided valuable information
- Jon Drukman and Mike Wertheim, who taught me how to produce audio
- Ted Rheingold, former IDEX volunteer and influential entrepreneur, who died of cancer in 2016
- Amor Potter, my lovely wife, who supported me in producing the web site
- Paul Strasburg, for bringing us all together in the first place

