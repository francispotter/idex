Title: Soundtrack

Use the player below to listen to the pre-recorded soundtrack for the slideshow presented at the 1996 anniversary dinner. We present it as it was recorded. You'll hear some background music and the voices of early participants sharing their memories. The slideshow lasted about 20 minutes, and we have not edited the soundtrack.

<audio src="/media/idex-10th-anniversary-dinner-soundtrack.mp3" controls></audio>

Based on some retained notes, a few memories, and educated guesses by those of us who produced the event, we have some knowledge of who you hear. We've cited their names and some information below, with question marks to fill the unknown voices.

We apologize in advance for mistakes and missing information! If you are one of the people who was interviewed for the soundtrack, and have additional information (such as recognizing your own voice), please [contact us](mailto:idex-soundtrack@fpotter.com).

<table class="table table-bordered table-striped table-condensed">
  <thead>
    <tr>
      <th>Speech</th>
      <th>Speaker</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>"I came back from Indonesia..."</td>
      <td>Rick Holmstrom, former board member</td>
    </tr>
    <tr>
      <td>"Actually, I first met Paul..."</td>
      <td>Margaret Schink, former board member, founder of the Shaler Adams Foundation and the Caritas Fund</td>
    </tr>
    <tr>
      <td>"I was lucky because...."</td>
      <td>Dwight Clark, former board member, founder of Volunteers in Asia</td>
    </tr>
    <tr>
      <td>"I entered Thailand about '65..."</td>
      <td>Kenny Lee, Peace Corps volunteer with Paul</td>
    </tr>
    <tr>
      <td>"I met Paul when he and I were fellow new students..."</td>
      <td>Anthony Lake, Former board member, Princeton classmate with Paul, former National Security Advisor</td>
    </tr>
    <tr>
      <td>"This was not a spur of the moment..."</td>
      <td>Kathleen Purcell, former board member, attorney</td>
    </tr>
    <tr>
      <td>"I just remember that there was this great enthusiasm..."</td>
      <td>Haleh Pourafzal, organizational consultant, former Education Director of Oxfam America</td>
    </tr>
    <tr>
      <td>"It was an evolution..."</td>
      <td>Rick Holmstrom</td>
    </tr>
    <tr>
      <td>"In the Spring..."</td>
      <td>Susan Beaudry, former staff member</td>
    </tr>
    <tr>
      <td>"The thing I remember most clearly..."</td>
      <td>Danilyn Rutherford, former staff member</td>
    </tr>
    <tr>
      <td>"In the very initial descriptions... and then the next step..."</td>
      <td>Kathleen Purcell</td>
    </tr>
    <tr>
      <td>"Although an organization can go through a great deal of change..."</td>
      <td>Haleh Pourafzal</td>
    </tr>
    <tr>
      <td>"IDEX is an organization that makes so little go such a long way..."</td>
      <td>Alexandra Rome, former board member</td>
    </tr>
    <tr>
      <td>"I also am very concerned about violence..."</td>
      <td>Margaret Schink</td>
    </tr>
    <tr>
      <td>"I think that IDEX is about as good and effective..."</td>
      <td>Anthony Lake</td>
    </tr>
    <tr>
      <td>"One of the communities that I visited in Ghana..."</td>
      <td>Susan Beaudry</td>
    </tr>
    <tr>
      <td>"These are very small amounts of resources..."</td>
      <td>Beth Foster, former staff member</td>
    </tr>
    <tr>
      <td>"And I think IDEX is learning..."</td>
      <td>Kathleen Purcell</td>
    </tr>
    <tr>
      <td>"Mutombo, Paul and I decided that..."</td>
      <td>Tim McMains, former board member</td>
    </tr>
    <tr>
      <td>"I put a bottle of beer on my head..."</td>
      <td>Mutombo Mpanya, former board member</td>
    </tr>
    <tr>
      <td>"Paul joined Charlie, Bob, and I..."</td>
      <td>Danilyn Rutherford</td>
    </tr>
    <tr>
      <td>"My sense was that IDEX was a learning organization..."</td>
      <td>Mutombo Mpanya</td>
    </tr>
    <tr>
      <td>"Volunteer meetings started out at Karie's apartment..."</td>
      <td>Danilyn Rutherford</td>
    </tr>
    <tr>
      <td>"IDEX wouldn't be IDEX without the volunteers..."</td>
      <td>Pam Kislak, former volunteer</td>
    </tr>
    <tr>
      <td>"Actually sometimes I forget I'm a volunteer..."</td>
      <td>Caroline Zodrow, former volunteer</td>
    </tr>
    <tr>
      <td>"One of the things that I remember very clearly..."</td>
      <td>Haleh Pourafzal</td>
    </tr>
    <tr>
      <td>"I must say it was great..."</td>
      <td>Tshilombo Ngoi</td>
    </tr>
    <tr>
      <td>"The Consortium for Global Development..."</td>
      <td>Cliff Chan, former Executive Director of Volunteers in Asia</td>
    </tr>
    <tr>
      <td>"When you start an organization..."</td>
      <td>Nazir Ahmad, former board member, co-founder of Overseas Development Network</td>
    </tr>
    <tr>
      <td>"I think one of the things that characterized..."</td>
      <td>Cliff Chan</td>
    </tr>
    <tr>
      <td>"I belive that the world has benefitted..."</td>
      <td>?</td>
    </tr>
    <tr>
      <td>"Paul, for many years to come, will remain an integral part of IDEX..."</td>
      <td>?</td>
    </tr>
    <tr>
      <td>"My first impressions of Paul..."</td>
      <td>Tim McMains</td>
    </tr>
    <tr>
      <td>"One thing about Paul..."</td>
      <td>Nazir Ahmad</td>
    </tr>
    <tr>
      <td>"We were supposed to talk about..."</td>
      <td>Pam Kislak</td>
    </tr>
    <tr>
      <td>"He was always a person..."</td>
      <td>Peter Bell, former Director of the Edna McConnel Clark Foundation, former Director of CARE</td>
    </tr>
    <tr>
      <td>"Just sort of a sparkling person..."</td>
      <td>Dave Brown, former board member</td>
    </tr>
    <tr>
      <td>"Paul is one of the most compassionate..."</td>
      <td>Susan Beaudry</td>
    </tr>
    <tr>
      <td>"I spent a lot of time in his home..."</td>
      <td>Steve Hellinger, former President of the Development GAP</td>
    </tr>
    <tr>
      <td>"He's not only gifted and smart and attractive..."</td>
      <td>Alexandra Rome</td>
    </tr>
    <tr>
      <td>"And what I would like to say to Paul..."</td>
      <td>Kenny Lee</td>
    </tr>
    <tr>
      <td>"Mr. Strasburg has been a wonderful person to us..." (and song)</td>
      <td>Mary Kpordotsi, former President of Helping Hand Association for Women’s Development, Ghana</td>
    </tr>
  </tbody>
</table>

We believe the voices of Karie Brown, former staff member, or Carolyn DePalatis, first paid staff member, might also appear in the soundtrack.

**[Look at the photos](/pages/photos.html)**
