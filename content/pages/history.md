Title: History
save_as: index.html

[Thousand Currents](https://thousandcurrents.org/) is an organization staffed by a diverse circle of passionate leaders, successfully partnering with grassroots groups and social movements worldwide to create loving, equitable, and just societies.

Since it's origin in the mid-1980s as IDEX (or International Development Exchange), the organization has channelled millions of US dollars toward its mission and connected with 200 million people worldwide.

In its first decade, IDEX raised small grants of 5 thousand dollars or less for grassroots women's cooperatives, youth organizations, and community groups in Africa, Asia, and Latin America. Run mostly by volunteers, it also operated an innovative education program in the US, teaching young people about the importance of global partnership and empowerment.

IDEX started with one person, Paul Strasburg, working with a community of family and friends to create a nonprofit that would outlast his own involvement. In 1996, IDEX organized a dinner event to mark the 10th anniversary of its establishment, and to honor Paul for his contribution. Part of the program was a slideshow with a pre-recorded soundtrack featuring the voices of early participants sharing their memories of the origins of this amazing group.

The slideshow was played once, at the anniversary event, and the surviving components are now available to the community in the form of this web site.

**[Listen to the Soundtrack](/pages/soundtrack.html)**
