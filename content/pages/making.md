Title: Making of the Slideshow
Slug: making

By Francis Potter

In mid-1996, Paula Morris and Martha Hannan asked me to create a slideshow to honor our dear friend, Paul Strasburg, on the 10th anniversary of IDEX. Before PowerPoint and desktop video production, slides were the best multimedia technology for affordable event presentations. As the former IDEX office manager, and fresh from a volunteer trip to Southern Africa to visit IDEX projects, I eagerly accepted the opportunity to assist.

The office had plenty of collected photos, so assembling a story from a selection was quick. However, we needed voices from early participants to recount their experiences. I traveled to meet subjects in the Bay Area, using a portable cassette recorder and microphone to capture interviews.

Reaching out to friends in other parts of the world posed a challenge. I connected a recording device to the phone in my small studio apartment and began making calls. Fortunately, the IDEX community responded warmly and enthusiastically, eager to share their stories.

Contacting Anthony Lake proved daunting. After attending Princeton with Paul, Anthony had risen in the government world to become the National Security Advisor to President Clinton. With trepidation, I phoned the White House. Miraculously, my call reached the Advisor's office. I explained our goal, assuring them that our conversation would focus solely on Paul and IDEX. They agreed, and we scheduled a 10-minute discussion. Remarkably, I ended up conversing with the serving National Security Advisor at the White House, and his anecdotes became an integral part of the IDEX tale.

Communicating with Africa in those days, before the internet and mobile phones, proved costly, unreliable, and often of poor quality. Nevertheless, we needed to reach an IDEX partner overseas, and Mary Kpardotsi in Ghana fit the bill. We sent a letter and eventually made the call. Mary surprised me with a beautiful song that became a highlight of the final presentation.

Armed with cassettes and slides, I had a couple of weeks to compile a meaningful show. The pinnacle of technology at the time was a slide dissolver—a device that alternated slides between two carousels, fading back and forth using a dimmer in each projector. Careful arrangement of the slides was necessary to alternate between the projectors, and we had to add a signal track to the audio to guide the dissolver during the show.

Thankfully, my musician friends taught me to produce the audio on a computer. We selected music, condensed the interviews, and merged everything into a single soundtrack with the slide dissolver cues. Luckily, I had access to a new CD burner and saved the soundtrack to a compact disc.

With the assistance of other volunteers, we set up the screen, audio system, speakers, dissolver, and slide carousels at Greens restaurant in San Francisco on the day of the dinner. However, a bug appeared in our well-planned scheme: one audio clip was missing from the CD. I had a cassette backup of the soundtrack and brought a cassette deck, so we proceeded with the lower-quality output and carried on with the show.

Afterward, all the slides were stored in a box at home, and I saved the audio file of the soundtrack. This website contains everything we still have, hopefully helping to convey the story of the origins of the remarkable organization that has evolved into Thousand Currents and has made a significant difference in countless lives, including my own.

**[Read the acknowledgements](/pages/thanks.html)**