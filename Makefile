.PHONY: clear init update test cover install style build publish

all: test cover style

# Set up a new venv
clear:
	rm -rf .venv
	python3.11 -m venv .venv

# Call this after adding a package to originals
add:
	.venv/bin/pip install -r requirements/original.txt
	.venv/bin/pip freeze > requirements/freeze.txt

# First time ever for this project, with no freezer
init: clear
	.venv/bin/pip install --upgrade -r requirements/original.txt
	.venv/bin/pip freeze > requirements/freeze.txt

# Local first time in the project, with a freezer
thaw: clear
	.venv/bin/pip install -r requirements/original.txt

# Run unit tests
test:
	.venv/bin/python -m unittest -v

# Check code coverage; fail if under 80%
cover:
	.venv/bin/python -m coverage run --source=busy --omit busy/__main__.py -m unittest -v > /dev/null 2>&1
	.venv/bin/python -m coverage report -m --fail-under 80

# Check code style - use AutoPEP8 to clean up some
style:
	.venv/bin/pycodestyle busy/**/*.py

# Build for Pip
build:
	mkdir -p dist
	rm -rf dist/*
	.venv/bin/python -m build

# Publish to PyPI
publish:
	.venv/bin/twine upload dist/*
